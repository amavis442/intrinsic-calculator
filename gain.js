"use strict";

let stdio = require('stdio');



let ops = stdio.getopt({
	'obv': { key: 'o', args: 1, mandatory: true, description: 'Old Book Value ($)' },
	'cbv': { key: 'c', args: 1, mandatory: true, description: 'Current Book Value ($)' },
	'years': { key: 'y', args: 1, mandatory: true, description: '# of Years Between Book Values' },
});

const gain = (currentBookValue, oldBookValue, years) => {
	let upper = parseFloat(1 / years);
	let base = parseFloat(currentBookValue / oldBookValue);
	let a = parseFloat(Math.pow(base, upper));
	let averageBookValueChange = parseFloat(100 * (a - 1)).toFixed(2); // Average Book Value Change (%)
	return averageBookValueChange
}

let currentBookValue = Number(ops.cbv);
let oldBookValue = Number(ops.obv);
let years = Number(ops.years);

let averageBookValueChange = gain(currentBookValue, oldBookValue, years);
console.log(averageBookValueChange + '%')