"use strict";

let stdio = require('stdio');

let ops = stdio.getopt({
	'dividend': { key: 'd', args: 1, mandatory: true, description: 'Cash Taken Out of Business ($): This is dividends recieved for 1 year.' },
	'current_book_value': { key: 'c', args: 1, mandatory: true, description: 'Current Book Value ($): We need to know this so we can determine the base value that\'s changing.' },
	'period_in_years': { key: 'p', args: 1, mandatory: true, description: 'Period: This will most likely be 10 (if you\'re comparing to a 10 year federal note).' },
	'discount_rate': { key: 'r', args: 1, mandatory: true, description: 'Low Risk investment like 10 Year Federal Note (%): Look up the ten year treasury note by clicking on this text.' },
	'average_book_value_change': { key: 'a', args: 1, mandatory: true, description: 'Average Percent Change in Book Value Per Year (%): This will determine the estimate Book Value at the end of the next 10 years.' },
});


const calculateIntrinsicValue = (averageBookValueChange, dividend, currentBookValue, periodInYears, discountRate) => {
	// (http://www.treasury.gov/resource-center/data-chart-center/interest-rates/Pages/TextView.aspx?data=yield)
	let perc = (1 + averageBookValueChange / 100);
	let base = Math.pow(perc, periodInYears);
	let cbvr = currentBookValue * base;
	discountRate = discountRate / 100;
	let extra = Math.pow((1 + discountRate), periodInYears);
	let c = dividend * (1 - (1 / extra)) / discountRate + cbvr / extra;

	return parseFloat(c).toFixed(2);  // Intrinsic Value ($): 
}

let dividend = Number(ops.dividend);
let currentBookValue = Number(ops.current_book_value);
let periodInYears = Number(ops.period_in_years);
let discountRate = Number(ops.discount_rate);
let averageBookValueChange = Number(ops.average_book_value_change);


let intrinsicValue = calculateIntrinsicValue(averageBookValueChange, dividend, currentBookValue, periodInYears, discountRate)
console.log(intrinsicValue);