"use strict";

let stdio = require('stdio');


function cur2float(user_input) {
    return parseFloat(cur2num(user_input));
}

function fndr(x) {
    var b = byfcf;
    var l = lgr;
    var gr1 = 1 + gr;
    var x1 = 1 + x;
    var m = n + 1;
    var r = Math.pow(gr1, m);
    var f0 = b * r * (1 + l) / (x - l) / Math.pow(x1, m) + b * (1 - Math.pow(gr1 / x1, m)) / (1 - gr1 / x1) - b - mp * cso;
    var fder = -b * (1 + l) * r * (1 / Math.pow(x1, m) / Math.pow(-l + x, 2) + m * Math.pow(x + 1, -1 - m) / (-l + x)) + b * (m * r * Math.pow(x1, -1 - m) / (1 - gr1 / x1) - gr1 * (1 - r / Math.pow(x1, m)) / Math.pow((1 - gr1 / x1) * x1, 2));
    var result = x - f0 / fder;

    return result;
}

function show_chart_1() {
    $("#wrapper-step1 .input-error").hide();
    byfcf = cur2float($('#txt-byfcf').val());
    gr = cur2float($('#txt-gr').val());
    n = cur2int($('#txt-n').val());
    var input_errors = false;
    if ((isNaN(byfcf)) || (byfcf <= 0)) {
        $("#error-byfcf").show();
        input_errors = true;
    }
    if ((isNaN(gr)) || (gr < 0) || (gr > 100)) {
        $("#error-gr").show();
        input_errors = true;
    }
    if ((isNaN(n)) || (n <= 0) || (n > 10)) {
        $("#error-n").show();
        input_errors = true;
    }
    if (!input_errors) {
        if (gr >= 1) {
            gr /= 100;
        }
        fcf = new Array();
        for (var i = 1; i <= n; i++) {
            fcf[i - 1] = Math.round(byfcf * Math.pow((1 + gr), i));
        }
        html = "<tr><td>Year</td>";
        for (var i = 1; i <= n; i++) {
            html += "<td>" + i.toString() + "</td>";
        }
        html += "</tr><tr><td>FCF</td>";
        for (var i = 1; i <= n; i++) {
            html += "<td>$" + fcf[i - 1].toString() + "</td>";
        }
        html += "</tr><tr><td>GR</td><td>" + (gr * 100).toFixed(2) + "%</td>";
        if (n > 1) {
            html += "<td colspan=\"" + (n - 1).toString() + "\">&nbsp;</td>";
        }
        html += "</tr>"; $("#wrapper-step1-results").html("<table class=\"calc-results\">" + html + "</table>");
        $("#wrapper-step1-results").show();
        $("#wrapper-step1-btn").hide(); $("#wrapper-step2").show();
    }
}

function show_chart_2() {
    show_chart_1();
    $("#wrapper-step2 .input-error").hide();
    dr = cur2float($('#txt-dr').val());
    var input_errors = false;
    if ((isNaN(dr)) || (dr < 0) || (dr > 100)) {
        $("#error-gr").show();
        input_errors = true;
    }
    if (!input_errors) {
        df = new Array();
        dfcf = new Array();
        sdfcf = 0;
        if (dr >= 1) {
            dr /= 100;
        }
        sdfcf = 0;
        for (var i = 1; i <= n; i++) {
            df[i - 1] = Math.pow((1 + dr), i).toFixed(2);
            dfcf[i - 1] = Math.round(fcf[i - 1] / df[i - 1]);
            sdfcf += dfcf[i - 1];
        }
        if (gr != dr) {
            sdfcf = Math.round(byfcf * (1 - Math.pow((1 + gr) / (1 + dr), n + 1)) / (1 - (1 + gr) / (1 + dr)) - byfcf);
        } else {
            sdfcf = Math.round(sdfcf);
        }
        html = "<tr><td>Year</td>";
        for (var i = 1; i <= n; i++) {
            html += "<td>" + i.toString() + "</td>";
        }
        html += "</tr><tr><td>FCF</td>";
        for (var i = 1; i <= n; i++) {
            html += "<td>$" + fcf[i - 1].toString() + "</td>";
        }
        html += "</tr><tr><td>DF</td>";
        for (var i = 1; i <= n; i++) {
            html += "<td>" + df[i - 1].toString() + "</td>";
        }
        html += "</tr><tr class=\"calc-results-hl\"><td>DFCF</td>";
        for (var i = 1; i <= n; i++) {
            html += "<td>$" + dfcf[i - 1].toString() + "</td>";
        }
        html += "</tr><tr>"; if (n > 2) {
            html += "<td colspan=\"" + (n - 2).toString() + "\">&nbsp;</td>";
        }
        if (n < 2) {
            html += "<td class=\"calc-results-hl\">Sum of DFCF</td>";
        } else {
            html += "<td class=\"calc-results-hl\" colspan=\"2\">Sum of DFCF</td>";
        }
        html += "<td class=\"calc-results-hl\">$" + sdfcf.toString() + "</td></tr>";
        $("#wrapper-step1-results").hide();
        $("#wrapper-step2-results").html("<table class=\"calc-results\">" + html + "</table>");
        $("#wrapper-step2-results").show();
        $("#wrapper-step2-btn").hide();
        $("#wrapper-step3").show();
        $('#span-years').html(n.toString());
    }
}

function show_chart_3() {
    show_chart_2();
    $("#wrapper-step3 .input-error").hide();
    lgr = cur2float($('#txt-lgr').val());
    var input_errors = false;
    if ((isNaN(lgr)) || (lgr < 0) || (lgr > 100)) {
        $("#error-lgr").show();
        input_errors = true;
    }

    if (!input_errors) {
        if (lgr >= 1) {
            lgr /= 100;
        }
        dpcf = Math.round(byfcf * Math.pow(1 + gr, n + 1) * (1 + lgr) / (dr - lgr) / Math.pow(1 + dr, n + 1));

        html += "<tr>"; if (n > 5) { html += "<td colspan=\"" + (n - 3).toString() + "\">&nbsp;</td>"; } else if (n > 2) { html += "<td colspan=\"" + (n - 2).toString() + "\">&nbsp;</td>"; }
        if (n == 1) { html += "<td class=\"calc-results-hl-final\">Discontinued perpetuity cash flow (DPCF)</td>"; } else if (n < 6) { html += "<td class=\"calc-results-hl-final\" colspan=\"2\">Discontinued perpetuity cash flow (DPCF)</td>"; } else { html += "<td class=\"calc-results-hl-final\" colspan=\"3\">Discontinued perpetuity cash flow (DPCF)</td>"; }
        html += "<td class=\"calc-results-hl-final\">$" + dpcf.toString() + "</td></tr>"; $("#wrapper-step2-results").hide(); $("#wrapper-step3-results").html("<table class=\"calc-results\">" + html + "</table>"); $("tr, td").removeClass("calc-results-hl"); $(".calc-results-hl-final").addClass("calc-results-hl"); $("#wrapper-step3-results").show(); $("#wrapper-step3-btn").hide(); $("#wrapper-step4").show();
    }
}

function show_chart_4() {
    show_chart_3(); $("#wrapper-step4 .input-error").hide(); cso = cur2int($('#txt-cso').val()); var input_errors = false; if ((isNaN(cso)) || (cso <= 0)) { $("#error-cso").show(); input_errors = true; }
    if (!input_errors) { ivs = (dpcf + sdfcf) / cso; }
    html = "<table class=\"calc-results\">" + html + "</table>"; html += "<br>"; html += "<p><em>The intrinsic value per share is $" + ivs.toFixed(2) + " at a " + (dr * 100).toFixed(2) + "% annual discount rate</em></p>"; $("#wrapper-step3-results").hide(); $("#wrapper-step4-results").html(html); $("tr, td").removeClass("calc-results-hl"); $(".calc-results-hl-final").addClass("calc-results-hl"); $("#wrapper-step4-results").show(); $("#wrapper-step4-btn").hide(); $("#wrapper-step5").show();
}

function show_chart_5() {
    show_chart_4(); $("#wrapper-step4 .input-error").hide(); mp = cur2float($('#txt-mp').val()); var input_errors = false; if ((isNaN(mp)) || (mp < 0)) { $("#error-mp").show(); input_errors = true; }
    if (!input_errors) {
        var eps = 0.0001; var test_html = ""; $("#wrapper-step6-results").html(""); for (var i = 0; i < 50; i++) { xa = xb = i / 100; var counter = 0; do { xa = xb; xb = fndr(xa); counter++; } while ((Math.abs(xb - xa) > eps) && (counter < 30) && (Math.abs(xb - xa) < 1)); if ((Math.abs(xb - xa) <= eps)) { break; } }
        if ((Math.abs(xb - xa) <= eps)) { var ndr = xb * 100; html += "<p><em>Based on the cash flows you have forecasted and a market price of $" + mp.toFixed(2) + ", this company may yield a " + ndr.toFixed(2) + "% annual return</em></p>"; } else { html += "It is not possible to calculate the annual return based on given set of data"; }
        $("#wrapper-step4-results").hide(); $("#wrapper-step5-results").html(html); $("tr, td").removeClass("calc-results-hl"); $(".calc-results-hl-final").addClass("calc-results-hl"); $("#wrapper-step5-results").show();
    }
}

function show_all() { show_chart_5(); }

function show_fcf_window() { $('#fcf-window').show(); }

function hide_fcf_window() { $('#fcf-window').hide(); }

function est_fcf_calculate() {
    var est_fcf = 0; var steps = 0; for (var i = 1; i <= 10; i++) { var fcf_step = cur2float($('#txt_fcf_' + i.toString()).val()); if (!isNaN(fcf_step)) { est_fcf += fcf_step; steps++; } }
    if (steps > 0) { est_fcf = Math.round(est_fcf / steps * 100) / 100; }
    $('#est-fcf').html('$' + est_fcf.toFixed(2)); return est_fcf;
}

function use_est_fcf() { var est_fcf = est_fcf_calculate(); $('#txt-byfcf').val(est_fcf); hide_fcf_window(); }

function show_gr_window() { $('#gr-window').show(); }

function hide_gr_window() { $('#gr-window').hide(); }

function est_gr_calculate() {
    var net_income = new Array(); var est_gr_step = new Array(); var est_gr = 0; var input_validated = true; var steps = 0; $('#est-gr-error').hide(); var net_income_past = cur2float($('#txt_income_past').val()); var net_income_current = cur2float($('#txt_income_current').val()); var year_past = cur2int($('#txt_year_past').val()); var year_current = cur2int($('#txt_year_current').val()); if (isNaN(net_income_past) || isNaN(net_income_current)) { $('#est-gr-error').html("Please enter valid net income values"); $('#est-gr-error').show(); input_validated = false; } else if (isNaN(year_past) || isNaN(year_current)) { $('#est-gr-error').html("Please enter valid years"); $('#est-gr-error').show(); input_validated = false; } else if (year_current == year_past) { $('#est-gr-error').html("Past year must be different than current year"); $('#est-gr-error').show(); input_validated = false; }
    if (input_validated) { est_gr = Math.round((Math.pow(net_income_current / net_income_past, 1 / (year_current - year_past)) - 1) * 10000) / 100; if (!isNaN(est_gr)) { $('#est-gr').html(est_gr.toFixed(2) + "%"); } else { $('#est-gr').html("N/A"); } } else { $('#est-gr').html("N/A"); }
    if (input_validated) { return est_gr; } else { return NaN; }
}

function use_est_gr() {
    est_gr = est_gr_calculate(); if (!isNaN(est_gr)) { $('#txt-gr').val(est_gr); }
    hide_gr_window();
}